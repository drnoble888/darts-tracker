from django.db import models


class Profile(models.Model):
    name = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    confirm_password = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    email = models.EmailField(max_length=200)
    picture = models.ImageField()


class Results(models.Model):
    profile = models.ForeignKey(
        Profile,
        games=models.IntegerField(default=0),
        wins=models.IntegerField(default=0),
        losses=models.IntegerField(default=0),
        related_name="results",
        on_delete=models.CASCADE
    )


class Players(models.Model):
    profile = models.ForeignKey(
        Profile,
        opponent=models.CharField(max_length=200),
        opponent_wins=models.IntegerField(default=0),
        opponent_losses=models.IntegerField(default=0),
        related_name="players",
        on_delete=models.CASCADE
    )
